import { Request, Response } from "express";

const notImplemented = (fn: Function, res: Response) => {
    return res.status(501).send(`${fn.name}: not implemented`);
}

class UserController {
    getUser = (req: Request, res: Response) => {
        return notImplemented(this.getUser, res)
    }

    createUser = (req: Request, res: Response) => {
        return notImplemented(this.createUser, res);
    }

    updateUser = (req: Request, res: Response) => {
        return notImplemented(this.updateUser, res);
    }

    deleteUser = (req: Request, res: Response ) => {
        return notImplemented(this.deleteUser, res);
    }

    getUsers = (req: Request, res: Response) => {
        return notImplemented(this.getUsers, res);
    }
}

export default UserController;