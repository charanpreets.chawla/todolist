import mongoose, { Schema } from 'mongoose';

const TodoSchema = new Schema({
    title: Schema.Types.String,
    description: Schema.Types.String,
    priority: Schema.Types.Number,
    tag: [Schema.Types.Number],
    deadline: Schema.Types.Date,
    belongsTo: [{ type: Schema.Types.ObjectId, ref: 'TdList' }]
})

const Todo = mongoose.model('Todo', TodoSchema);
export default Todo;