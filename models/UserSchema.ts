import mongoose, { Schema } from 'mongoose';

const UserSchema = new Schema({
    name: Schema.Types.String,
    email: Schema.Types.String,
    bDay: Schema.Types.Date,
    tdList: [{ type: Schema.Types.ObjectId, ref: 'TDList' }]
})

const User = mongoose.model('User', UserSchema);
export default User;