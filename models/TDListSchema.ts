import mongoose, { Schema } from 'mongoose';

const TdListSchema = new Schema({
    belongsTo: { type: Schema.Types.ObjectId, ref: 'User' },
    sharedWith: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    todos: [{ type: Schema.Types.ObjectId, ref: 'Todo' }],
    subTdList: { type: Schema.Types.ObjectId, ref: 'TdList' },
})

const TdList = mongoose.model('TdList', TdListSchema);
export default TdList;