```mermaid
erDiagram
    USER ||--o{ TDLIST : todolists
    USER {
        number id PK "*"
        string name
        string email
        Date   bDay
        TDLIST[] todoLists
        Date createdOn "*"
        Date updatedOn "*"
    }
    TDLIST |o--o{ USER : sharedWith
    TDLIST |o--o| TDLIST : subTdList
    TDLIST{
        number id PK "*"
        USER belongsTo "*"
        USER[] sharedWith
        TODO[] todos
        TDLIST subTdList
        Date createdOn "*"
        Date updatedOn "*"
    }
    TODO }o--|| TDLIST : belongsTo
    TODO{
        number id PK "*"
        string title "*"
        string description
        number priority
        number[] tag
        Date deadline
        TDLIST belongsTo "*"
        Date createdOn "*"
        Date updatedOn "*"s
```
